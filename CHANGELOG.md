# Config Server ChangeLog

## 1.1.0

* Allow multiple user account for authentication with config server (see [EOS-3192](https://sothebys.jira.com/browse/EOS-3192)).
* Update to Spring Cloud Edgware release train.
* Update to Spring Boot version 1.5.10.

## 1.0.0

* Initial version.
