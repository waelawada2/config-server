FROM openjdk:8-jre-alpine
ADD target/config-server*.jar app.jar
EXPOSE 8888
ENV PORT=8888
ENV JAVA_OPTS="-Xmx384M"
ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /app.jar" ]
