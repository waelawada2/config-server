package com.sothebys.config.service;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import com.sothebys.config.configuration.ConfigServerConfiguration;
import com.sothebys.config.configuration.ConfigServerUser;
import java.util.Arrays;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * Tests for {@link PropertiesUserDetailsService}
 *
 * @author Gregor Zurowski
 */
public class PropertiesUserDetailsServiceTest {

  private static final List<ConfigServerUser> USERS = Arrays.asList(new ConfigServerUser[]{
      new ConfigServerUser("gcostanza", "season1!", "USER"),
      new ConfigServerUser("jseinfeld", "season2#", "USER")
  });

  private PropertiesUserDetailsService propertiesUserDetailsService;

  @Before
  public void setUp() {
    ConfigServerConfiguration configServerConfiguration = new ConfigServerConfiguration();
    configServerConfiguration.setUsers(USERS);
    this.propertiesUserDetailsService = new PropertiesUserDetailsService(configServerConfiguration);
  }

  /**
   * Tests that a valid user returns a valid {@link UserDetails} instance.
   */
  @Test
  public void testValidUser() {
    UserDetails userDetails = propertiesUserDetailsService.loadUserByUsername("jseinfeld");
    assertThat(userDetails.getUsername(), is("jseinfeld"));
    assertThat(userDetails.getPassword(), is("season2#"));
    assertTrue(userDetails.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_USER")));
  }

  /**
   * Tests that an invalid user triggers a {@link UsernameNotFoundException} with the expected error
   * message.
   */
  @Test
  public void testInvalidUser() {
    UsernameNotFoundException unfe = null;
    try {
      propertiesUserDetailsService.loadUserByUsername("ckramer");
    } catch (UsernameNotFoundException e) {
      assertThat(e.getMessage(), is("User not found: ckramer"));
      unfe = e;
    }
    assertNotNull(unfe);
  }

}
