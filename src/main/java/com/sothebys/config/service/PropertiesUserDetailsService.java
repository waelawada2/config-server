package com.sothebys.config.service;

import com.sothebys.config.configuration.ConfigServerConfiguration;
import com.sothebys.config.configuration.ConfigServerUser;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * A custom properties-based {@link UserDetailsService} that allows to configure
 * multiple users for the default authentication mechanism.
 *
 * @see <a href="https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#howto-change-the-authenticationmanager-and-add-user-accounts">Change the AuthenticationManager and Add User Accounts</a>
 *
 * @author Gregor Zurowski
 */
@Service
public class PropertiesUserDetailsService implements UserDetailsService {

  private List<ConfigServerUser> users;

  @Autowired
  public PropertiesUserDetailsService(ConfigServerConfiguration configServerConfiguration) {
    this.users = configServerConfiguration.getUsers();
  }

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    Optional<ConfigServerUser> userOptional = users.stream()
        .filter(u -> u.getUsername().equals(username))
        .findAny();
    if (!userOptional.isPresent()) {
      throw new UsernameNotFoundException("User not found: " + username);
    }

    ConfigServerUser user = userOptional.get();

    return User
        .withUsername(user.getUsername())
        .password(user.getPassword())
        .roles(user.getRole())
        .build();
  }

}
