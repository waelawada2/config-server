package com.sothebys.config.configuration;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Represents a config server user entity.
 *
 * @author Gregor Zurowski
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ConfigServerUser {

  private String username;

  private String password;

  private String role;

}
