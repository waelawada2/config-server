package com.sothebys.config.configuration;

import java.util.List;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration properties for the config server.
 *
 * @author Gregor Zurowski
 */
@Data
@Configuration
@ConfigurationProperties("config-server")
public class ConfigServerConfiguration {

  private List<ConfigServerUser> users;

}
